public class Figure {

    protected int coorX;
    protected int coorY;
    protected double a;
    protected double b;

    public Figure(int coorX, int coorY, double a, double b) {
        this.coorX = coorX;
        this.coorY = coorY;
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return 0;
    }

    public void paint() {
        System.out.println("Фигура на координатах x(" + coorX + "), y(" + coorY + ")");
    }
}
