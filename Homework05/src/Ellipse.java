public class Ellipse extends Figure{

    public Ellipse(int coorX, int coorY, double a, double b) {
        super(coorX, coorY, a, b);
    }

    public double getPerimeter() {
        return 2 * 3.14 * Math.sqrt((a * a + b * b) / 2);
    }

    public void paint() {
        System.out.println("Эллипс на координатах x(" + coorX + "), y(" + coorY + ")");
        System.out.println("С периметром = " + getPerimeter());
    }
}
