public class Square extends Rectangle{

    public Square(int coorX, int coorY, double a) {
        super(coorX, coorY, a, a);
    }

    public double getPerimeter() {
        return a * 4;
    }

    public void paint() {
        System.out.println("Квадрат на координатах x(" + coorX + "), y(" + coorY + ")");
        System.out.println("С периметром = " + getPerimeter());
    }

}
