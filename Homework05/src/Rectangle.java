public class Rectangle extends Figure{

    public Rectangle(int coorX, int coorY, double a, double b) {
        super(coorX, coorY,a , b);
    }

    public double getPerimeter() {
        return 2 * a + 2 * b;
    }

    public void paint() {
        System.out.println("Прямоугольник на координатах x(" + coorX + "), y(" + coorY + ")");
        System.out.println("С периметром = " + getPerimeter());
    }
}
