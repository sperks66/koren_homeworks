//Сделать класс Figure, у данного класса есть два поля - x и y координаты.
//
//        Классы Ellipse и Rectangle должны быть потомками класса Figure.
//
//        Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
//
//        В классе Figure предусмотреть метод getPerimeter(), который возвращает 0. Во всех остальных классах он должен возвращать корректное значение.

public class Main {

    public static void main(String[] args) {

        Figure figure = new Figure(1, 2, 2.0, 3.0);
        Ellipse ellipse = new Ellipse(0, 0, 2.0, 3.0);
        Rectangle rectangle = new Rectangle(3, 4, 2.0, 3.0);
        Circle circle = new Circle(5, 6, 2.0);
        Square square = new Square(7, 8, 2.0);

        figure.paint();
        ellipse.paint();
        rectangle.paint();
        circle.paint();
        square.paint();

    }
}
