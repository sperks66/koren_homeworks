import java.util.Arrays;

public class Homework03 {

    public static void sortArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != -1){
            int min = array[i];
            int minIndex = i;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] < min & array[j] != -1) {
                        min = array[j];
                        minIndex = j;
                    }
                }
                int temp = array[i];
                array[i] = array[minIndex];
                array[minIndex] = temp;
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static int searchRareNumber(int[] array) {

        int minCount = 2147483647;
        int rareNumber = -100;
        for (int i = 0; array[i] != -1; i++){
            int count = 0;
            while (array[i] == array[i +1]) {
                count++;
                i++;
            }
            if (minCount > count) {
                minCount = count;
                rareNumber = array[i];
            }
        }
        return rareNumber;
    }

    public static void main(String[] args) {
        int[] a = {-5, 10, -5, -30, -30, 10, 80, 40, 15, -5, -85, -85, -50, 40, 15, -20, -20, -50, -1};
        sortArray(a);
        System.out.println(searchRareNumber(a));

    }
}
