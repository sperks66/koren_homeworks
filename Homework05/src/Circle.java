public class Circle extends Ellipse{

    public Circle(int coorX, int coorY, double a) {
        super(coorX, coorY, a, a);
    }

    public double getPerimeter() {
        return 2 * 3.14 * a;
    }

    public void paint() {
        System.out.println("Круг на координатах x(" + coorX + "), y(" + coorY + ")");
        System.out.println("С периметром = " + getPerimeter());
    }
}
